# GiteaBrowserHelpers

These are some Tampermonkey scripts i find convenient

## Installation

1. Install [Tampermonkey](https://tampermonkey.net/)
1. Select a script in this repo that you wish to use. View the file and click the _Raw_ button at the top of the file to view its source
1. Copy the source
1. Open Tampermonkey in your browser and click the Add Script tab (icon with a plus symbol)
1. Paste the source into the script window and hit save
1. Voila!

## Buildbot Buttons

Can't remember the exact syntax for the buildbots? yeah... me neither... this one adds some buttons to help with the most common tasks for the bots. 

![BuildBotButtons](./img/BuildBotButtons.png) 
