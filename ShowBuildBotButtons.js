// ==UserScript==
// @name         Show buildbot buttons on projects.blender.org
// @namespace    http://www.lazydodo.com/
// @version      0.2
// @description  Show build/package shortcut buttons on a diff
// @author       Ray
// @match        https://projects.blender.org/blender/blender/*
// @run-at       document-idle
// ==/UserScript==
/* global $, VM */

function doBuild()
{
   var $edit = $(".markdown-text-editor").val("@blender-bot build")
}

function doPackage()
{
   var $edit = $(".markdown-text-editor").val("@blender-bot package")
}

(function() {
    $("<div class=\"ui segment text right\"><div><button class=\"right ui small button blue\" id=\"Build\">Build</button><button class=\"right ui small button blue\" id=\"Package\">Package</button></div></div>").insertAfter('#comment-form');
    $("#Build").click (doBuild);
    $("#Package").click (doPackage);
})();

